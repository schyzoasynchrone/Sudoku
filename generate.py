#!/usr/bin/env python3
# encoding:utf8

from random import randrange, choice
from core.board import Board
from solver import solve, solve_hard, solve_extreme

def generate(difficulty="easy"):
    """Return a randomly generated Board.
    The difficulty determines which solving function will be used to decide
    wether the board is solvable or not"""
    solve_function = {"easy" : solve,
                    "normal" : solve_hard,
                    "difficult" : solve_extreme}
    success = False
    while not success:
        success = True
        board = Board()
        for _ in range(15):
            try:
                cell_random(board)
            except IndexError:
                success = False
        try:
            clone = board.copy()
            clone = solve_function[difficulty](clone)
        except ValueError:
            success = False
    i = 0
    while not clone.is_complete():
        if i > 20:
            return generate()
        save = board.copy()
        try:
            cell_random(board)
            clone = board.copy()
            clone = solve_function[difficulty](clone)
        except ValueError:
            i += 1
            board = save
            clone = board.copy()
        except IndexError:
            board = save
            clone = board.copy()
    return board

def cell_random(board):
    """Gives a random value to a random cell in board
    Raises a ValueError in case the selected cell has no possible values"""
    i, j = randrange(9), randrange(9)
    while board[i,j].value:
        i, j = randrange(9), randrange(9)
    possible_values = [i for i in range(1, 10)]
    for cell in board.get_row(i):
        if cell.value:
            try: possible_values.remove(cell.value)
            except ValueError: pass
    for cell in board.get_column(j):
        if cell.value:
            try: possible_values.remove(cell.value)
            except ValueError: pass
    for cell in board.get_square(i//3,j//3):
        if cell.value:
            try: possible_values.remove(cell.value)
            except ValueError: pass
    value = choice(possible_values)
    board[i,j].set_value(value)
    board[i,j].set_mutable(False)


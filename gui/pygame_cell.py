#!/usr/bin/env python3
# encoding:utf-8

from core.cell import Cell

import pygame

class PyCell(Cell, pygame.sprite.Sprite):
    """Class of visible cell."""

    COLOR = {"white":(255,255,255),
            "black":(0,0,0),
            "yellow":(255,255,0),
            "grey":(125,125,125),
            "blue":(0,0,255),
            "red":(255,0,0),
            }

    def __init__(self, size, position):
        # On appelle les init des deux parents
        Cell.__init__(self)
        pygame.sprite.Sprite.__init__(self)

        self.show_guess = False
        self.wrong = False
        self.guess = dict()
        for i in range(9):
            self.guess[i] = 0
        self.selected_guess = 0

        # Create the text
        self.value_font = pygame.font.Font(None, size[1]//2)
        self.guess_font = pygame.font.Font(None, size[1]//3)
        self.color = self.COLOR["white"]

        # Creates the surface
        self.set_size(size)
        # Add the text to the surface
        self.update()
        # Get the rect
        self.set_position(position)

    def update(self):
        size = width, height = self.image.get_size()
        if not self.wrong: self.image.fill(self.color)
        else: self.image.fill(self.COLOR["red"])
        if not self.show_guess:
            if self.value:
                text = self.value_font.render(str(self.value), True, (0,0,0))
                self.image.blit(text, (width//4,height//4))
        else:
            for i in range(len(self.guess)):
                if i == self.selected_guess and self.is_selected:
                    background = pygame.Surface((width//3,height//3))
                    background.fill(self.COLOR["blue"])
                    self.image.blit(background, (width//3 * (i%3), height//3 *
                        (i//3)))
                if self.guess[i]:
                    text = self.guess_font.render(str(self.guess[i]), True,
                            self.COLOR["black"])
                    self.image.blit(text, ((width//6)+ width//3 * (i%3), 
                        (height//12) + height//3 * (i//3)))

    def set_size(self, size):
        self.image = pygame.Surface(size)
        self.image.fill(self.color)
        self.rect = self.image.get_rect()

    def set_position(self, position):
        self.rect.center = position

    def set_selected(self, value):
        if value:
            self.color = self.COLOR["yellow"]
        else:
            self.color = self.COLOR["white"]
        self.is_selected = value
        
    def set_mutable(self, value):
        Cell.set_mutable(self, value)
        if not self.is_mutable:
            self.color = self.COLOR["grey"]

    def toggle_display(self):
        self.show_guess = not self.show_guess

    def change_guess(self, direction):
        if direction == "up" and self.selected_guess > 2:
            self.selected_guess -= 3
        elif direction == "down" and self.selected_guess < 6:
            self.selected_guess += 3
        elif direction == "left" and self.selected_guess not in range(0,9,3):
            self.selected_guess -= 1
        elif direction == "right" and self.selected_guess not in range(2,9,3):
            self.selected_guess += 1

    def set_wrong(self, value):
        self.wrong = value

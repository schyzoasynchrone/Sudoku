#!/usr/bin/env python3
# encoding:utf-8

from core.board import Board
from gui.pygame_cell import PyCell

import pygame

class PyBoard(Board, pygame.sprite.Group):
    """Class of visible board."""
    def __init__(self, size):
        # On appelle l’init des parents
        pygame.sprite.Group.__init__(self)
        width, height = size

        # Init cells
        self.cells = {}
        cell_size = (width//9,height//9)
        for i in range(9):
            for j in range(9):
                x = cell_size[0] * j + cell_size[0] // 2
                y = cell_size[1] * i + cell_size[1] // 2
                cell_position = (x,y)
                self.cells[(i,j)] = PyCell(cell_size, cell_position)
                self.add(self[(i,j)])

        # Init borders
        self.limits = pygame.sprite.Group()
        borders_size = cell_size[0] // 10, height
        for i in range(10):
            borders_position = cell_size[0] * i, 0
            self.limits.add(VertLimit(borders_size, borders_position, 
                not i % 3))
        borders_size = width, cell_size[1] // 10
        for j in range(10):
            borders_position = 0, cell_size[1] * j
            self.limits.add(HoriLimit(borders_size, borders_position, 
                not j % 3))

class Limit(pygame.sprite.Sprite):
    """Border of cells"""
    def __init__(self, size, position, big=False):
        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.Surface(size)
        self.image.fill((0,0,0))

        self.rect = self.image.get_rect()

class VertLimit(Limit):
    """Vertical Border of cells"""
    def __init__(self, size, position, big= False):
        size = list(size)
        if big:
            size[0] = 2 * size[0]
        Limit.__init__(self, size, position, big)
        self.rect.midtop = position

class HoriLimit(Limit):
    """Horizontal Border of cells"""
    def __init__(self, size, position, big= False):
        size = list(size)
        if big:
            size[1] = 2 * size[1]
        Limit.__init__(self, size, position, big)
        self.rect.midleft = position


#!/usr/bin/env python3
# encoding:utf8

class Cell(object):
    """Cell in a Sudoku game"""
    def __init__(self):
        self.value = 0
        self.guess = list()
        self.is_mutable = True
        
    def set_value(self, value):
        """value is an integer between 1 and 9, 
        if not, a ValueError is raised"""
        if value in tuple(x for x in range(10)):
            self.value = value
        else:
            raise ValueError
            
    def set_guess(self, guess):
        """guess is a list containing integers between 
        1 and 9, if not, a ValueError is raised"""
        for value in guess:
            if value not in tuple(x for x in range(1,10)):
                raise ValueError
        self.guess = guess 
        
    def set_mutable(self, value):
        """value is a boolean"""
        self.is_mutable = value

    def __str__(self):
        return self.value, self.guess

    def copy(self):
        """Copy the cell to another object"""
        clone = Cell()
        clone.set_value(self.value)
        clone.set_guess(self.guess.copy())
        clone.set_mutable(self.is_mutable)
        return clone

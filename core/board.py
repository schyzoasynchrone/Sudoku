from core.cell import Cell

class BoardError(ValueError):
    """Error in a board, like two cells having the same value in the same
    row"""
    def __init__(self, cell1, cell2):
        super().__init__()
        self.cells = (cell1, cell2)

class Board(object):
    """Board in a Sudoku Game"""
    def __init__(self):
        self.cells = {}
        for i in range(9):
            for j in range(9):
                self.cells[(i,j)] = Cell()

    def get_row(self, index):            
        """Returns a list containing the cells in the selected row"""
        return [self.cells[(index,j)] for j in range(9)]
                
    def get_column(self, index):
        """Returns a list containing the cells in the selected column"""
        return [self.cells[(i,index)] for i in range(9)]            
                
    def get_square(self, n, m):
        """Returns a list containing the cells in the selected square.
        n is the row number of the square and m is its column number"""             
        square = list()
        for i in range(n*3, 3*(n+1)):
            for j in range(m*3, 3*(m+1)):        
                square.append(self.cells[(i,j)])    
        return square     
                
    def set_value(self, n, m, value):
        """Set the value of the cell in row n and column m"""                
        self.cells[(n,m)].set_value(value)                
                
    def set_guess(self, n, m, guess):
        """Set the guess of the cell in row n and column m"""
        self.cells[(n,m)].set_guess(guess)
          
    def __getitem__(self, index):
        """Returns the cell of the selected index.
        Board expects index to be a iterable of size two"""            
        if len(index) != 2:
            raise IndexError
        return self.cells[index]

    def __setitem__(self, index, value):
        """Board expects index to be an iterable of size two"""
        if len(index) != 2:
            raise IndexError
        self.cells[index] = value
                
    def load_config(self, config_file):
        """calls the parsing method with a sudoku file"""
        with open(config_file, 'r') as file:
            self.parse_config(file.read())

    def parse_config(self, text):
        """Changes the cells values according to a string of 90 characters"""
        """Takes a string as input"""
        i=0
        for j in range(len(text)):
            char = text[j]
            if char not in ("0","1","2","3","4","5","6","7","8","9","\n"):
                raise ValueError
            if char != "\n":
                value = int(char)
                self.set_value(i, j%10, value )
                if value != 0:
                    self[i,j%10].set_mutable(False)
            else:
                i+=1
                    
    def check_cell_error(self):
        """Checks the cells
        Raises a BoardError in case there is an error, like two cells in the
        same row with the same value"""
        # Checking the rows
        for i in range(9):
            for j in range(9):
                if self[i,j].value != 0:
                    for k in range(j+1,9):
                        if self[i,j].value == self[i,k].value:
                            raise BoardError(self[i,j],self[i,k])
                        
        # Checking the columns
        for j in range(9):
            for i in range(9):
                if self[i,j].value != 0:
                    for k in range(i+1,9):
                        if self[i,j].value == self[k,j].value:
                            raise BoardError(self[i,j],self[k,j])
                        
        # Checking the squares
        # Can be improved, since currently each cell is checked twice with
        # another
        for n,m in [(x,y) for x in range(3) for y in range(3)]:
            for i in self.get_square(n,m):
                if i.value != 0:
                    for j in self.get_square(n,m):
                        if j != i and i.value == j.value:
                            raise BoardError(i, j)

    def __str__(self):
        string = str()
        # Start by the column because we first want the lines
        for i in range(9):
            for j in range(9):
                string += str(self[i,j].value)
            string += "\n"
        return string

    def __iter__(self):
        for i in range(9):
            for j in range(9):
                yield self[(i,j)]

    def __repr__(self):
        string = "B'"
        for i in range(9):
            for j in range(9):
                string += str(self[i,j].value)
            string += "\\n"
        string += "'"
        return string

    def copy(self):
        """Copy the Board to another object"""
        clone = Board()
        for i in range(9):
            for j in range(9):
                clone[i,j] = self[i,j].copy()
        return clone

    def is_complete(self):
        """Returns True if all cells have a value, False otherwise"""
        completed = True
        for cell in self:
            if not cell.value:
                return not completed
        return completed

#!/usr/bin/env python3
# encoding:utf-8

from gui.pygame_board import PyBoard
from core.board import Board, BoardError
from solver import solve, solve_hard, solve_extreme
from generate import generate
#from Sudoku.gui.pygame_cell import PyCell

import pygame
import sys
import argparse

pygame.init()
CONFIG = ("351000070\n"
          "400502930\n"
          "200047560\n"
          "020070006\n"
          "006850092\n"
          "045000107\n"
          "034001025\n"
          "518020600\n"
          "000465800\n") 

class Game(object):
    """The game controller"""

    NUMBER_KEYS = {pygame.K_0 : 0, pygame.K_KP0 : 0,
            pygame.K_1 : 1, pygame.K_KP1 : 1, 
            pygame.K_2 : 2, pygame.K_KP2 : 2, 
            pygame.K_3 : 3, pygame.K_KP3 : 3,
            pygame.K_4 : 4, pygame.K_KP4 : 4, 
            pygame.K_5 : 5, pygame.K_KP5 : 5,
            pygame.K_6 : 6, pygame.K_KP6 : 6,
            pygame.K_7 : 7, pygame.K_KP7 : 7,
            pygame.K_8 : 8, pygame.K_KP8 : 8,
            pygame.K_9 : 9, pygame.K_KP9 : 9,
            pygame.K_DELETE : 0}
    MOVE_KEYS = {pygame.K_LEFT : "left", pygame.K_RIGHT : "right",
            pygame.K_UP : "up", pygame.K_DOWN : "down"}

    black = (0,0,0)
    white = (255,255,255)

    def __init__(self, size):
        """Creates a game object, used to control what is happening.
        Uses PyGame"""
        self.size = self.width, self.height = size
        self.difficulty = "easy"

        self.screen = pygame.display.set_mode(self.size)
        self.board = PyBoard(size)
        self.selected_cell = None

        self.update()

    def start(self):
        """Starts the main loop"""
        while True:
            need_update = False
            for event in pygame.event.get():
                # Quitter le programme
                if event.type == pygame.QUIT: sys.exit()
                # Sélectionner une cellule
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    need_update = True
                    pos = pygame.mouse.get_pos()
                    for cell in self.board:
                        if cell.rect.collidepoint(pos):
                            self.select(cell)
                # Entrer un nombre
                elif event.type == pygame.KEYDOWN:
                    need_update = True
                    if event.key in self.NUMBER_KEYS:
                        if self.selected_cell:
                            if self.selected_cell.show_guess:
                                self.selected_cell.guess[self.selected_cell.selected_guess] = self.NUMBER_KEYS[event.key]
                            else:
                                for cell in self.board:
                                    cell.set_wrong(False)
                                self.selected_cell.set_value(
                                        self.NUMBER_KEYS[event.key])
                                if (self.difficulty == "easy"
                                        or (self.difficulty == "normal"
                                            and self.board.is_complete())):
                                    try:
                                        self.board.check_cell_error()
                                    except BoardError as failure:
                                        for cell in failure.cells:
                                            cell.set_wrong(True)
                    if event.key == pygame.K_SPACE:
                        if self.selected_cell:
                            self.selected_cell.toggle_display()
                    if event.key in self.MOVE_KEYS:
                        if self.selected_cell:
                            if self.selected_cell.show_guess:
                                self.selected_cell.change_guess(
                                        self.MOVE_KEYS[event.key])

            # Mettre à jour l’affichage
            if need_update: self.update()

    def update(self):
        """Updates the display"""
        self.board.update()
        self.screen.fill(self.black)
        self.board.draw(self.screen)
        self.board.limits.draw(self.screen)
        pygame.display.flip()

    def load_config(self, path):
        """Load a config.
        Uses Board’s load_config method"""
        self.board.load_config(path)
        self.update()

    def parse_config(self, conf):
        """Parse a config.
        Uses Board’s parse_config method"""
        self.board.parse_config(conf)
        self.update()

    def select(self, cell):
        """Selects a cell to be modified"""
        if self.selected_cell:
            self.selected_cell.set_selected(False)
            self.selected_cell = None
        if cell.is_mutable:
            self.selected_cell = cell
            cell.set_selected(True)

    def set_difficulty(self, value="easy"):
        """Sets game difficulty.
        Only influences when errors in the board are checked:
        - easy: every time a value is entered
        - normal: only when all cells have a value"""
        if value in ("easy","normal"):
            self.difficulty = value


if __name__ == "__main__":
    parser =  argparse.ArgumentParser(description="A sudoku game")
    parser.add_argument("-l", "--load", type=open, help="the configuration"
        " file to be opened")
    parser.add_argument("-s", "--solve", action="store_true", help="if on, the"
    " game is not started and the sudoku is solved")
    parser.add_argument("--difficult", "-d", action="store_true", help="if the"
    " solving failed, try this")
    parser.add_argument("--extrem", "-e", action="store_true", help="in case "
    "nothing else works")

    args = parser.parse_args()
    if args.solve:
        board = Board()
        if args.load:
            board.parse_config(args.load.read())
        else:
            board.parse_config(CONFIG)
        if args.difficult:
            solve_hard(board)
        elif args.extrem:
            board = solve_extreme(board)
        else:
            solve(board)
        print(board)
        sys.exit()

    size = 640,480
    game = Game(size)
    if args.difficult or args.extrem:
        game.set_difficulty("normal")
    if args.load:
        game.parse_config(args.load.read())
    else:
        if args.difficult:
            config = str(generate(difficulty="normal"))
        elif args.extrem:
            config = str(generate(difficulty="difficult"))
        else:
            config = str(generate())
        game.parse_config(config)
    game.start()

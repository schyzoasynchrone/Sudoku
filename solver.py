#/usr/bin/env python3
# encoding:utf8
#rule 2 and rule 3 4 still both need to be checked and that will be it

#vérifier si la config a bien les bons guess et les bons booléens après le 
#passage unique de init

def init_guesses(board):
    """Sets all guesses full for undecided cells
    An undecided cell is a cell with no value.
    A full guess is a guess containing all integers between 1 and 9"""
    for i in board:
        if i.value == 0:
            liste = [j for j in range(1,10)]
            i.set_guess(liste)
            i.set_mutable(True)
        else:
            i.set_mutable(False) 

def remove_guesses(board):
    """Remove guesses of the cells in board according to the values of the
    cells around.
    Basically, it just runs across all cells and remove their value from
    surrounding cells’ guesses.
    It does not modify cells’ value"""
    for n in range(9):
        for m in range(9):
            if not board[n,m].is_mutable:
                for i in board.get_row(n):
                    if i.is_mutable:
                        liste = i.guess
                        try:
                            liste.remove(board[n,m].value)
                        except ValueError: pass
                        i.set_guess(liste)
                for i in board.get_column(m):
                    if i.is_mutable:
                        liste = i.guess
                        try:
                            liste.remove(board[n,m].value)
                        except ValueError: pass
                        i.set_guess(liste)
                for i in board.get_square(n//3,m//3):
                    #division to check
                    if i.is_mutable:
                        liste = i.guess
                        try:
                            liste.remove(board[n,m].value)
                        except ValueError: pass
                        i.set_guess(liste)
                        
def check_guesses(board):
    """If a cell in board has only one possible value 
    (i.e. len(cell.guess) == 1) it is given that value
    It is the only function modifying cells’ values"""
    #fonction qui check if ever un guess apparaît seul, pour set la value de la 
    #cell en question
    #il faut absolument remove guesses avant de check guesses
    mod = 0
    for i in board:
        if i.is_mutable:
            if len(i.guess) == 1:
                i.set_value(i.guess[0])
                i.guess.pop(0)
                i.set_mutable(False)
                mod = 1
            elif len(i.guess) == 0:
                raise ValueError
    return not mod
    
def rule1dot5(board):
    """If a cell is the only one in a group (meaning a square, a row or a
    column) that has a specific value in its guesses, then its guesses are set
    to this very value"""
    a = 0
    for n,m in [(x,y) for x in range(3) for y in range(3)]:
        for c in range(1,10):
            for i in board.get_square(n,m):
                if c in i.guess:
                    a += 1
            if a == 1:
                for i in board.get_square(n,m):
                    if c in i.guess:
                        i.set_guess([c])
            a = 0
    for k in range(9):
        for c in range(1,10):
            for i in board.get_row(k):
                if c in i.guess:
                    a += 1
            if a == 1:
                for i in board.get_row(k):
                    if c in i.guess:
                        i.set_guess([c])
            a = 0
            for i in board.get_column(k):
                if c in i.guess:
                    a += 1
            if a == 1:
                for i in board.get_column(k):
                    if c in i.guess:
                        i.set_guess([c])
            a = 0
    
def rule2(board):
    """rule 2 regarde si des couples ou trouples sont présents dans un domaine
    si oui, on élimine les guess en trop qui ne peuvent logiquement
    pas être dans le set de couple ou trouple"""
    
    a=0
    #couple checking part
    for c1,c2 in [(x,y) for x in range(1,10) for y in range(1,10) if x>y]:
        #pour deux chiffres donnés (oui je check tous les couples de chiffres 
        #entre 1 et 9)
        for n in range(9):
            #pour un rang de colonne ou ligne donné on check les occurences de 
            #c1 et c2 dedans
            for i in board.get_row(n):
                if c1 in i.guess and c2 in i.guess:
                    a+=1
                if c1 in i.guess and c2 not in i.guess or c1 not in i.guess and c2 in i.guess:
                    a+=3
            if a==2:
                for i in board.get_row(n):
                    if c1 in i.guess and c2 in i.guess:
                        item=0
                        while item<len(i.guess):
                            if i.guess[item]!=c1 and i.guess[item]!=c2:
                                i.guess.remove(i.guess[item])
                            else:
                                item+=1
            a=0
            for i in board.get_column(n):
                if c1 in i.guess and c2 in i.guess:
                    a+=1
                if c1 in i.guess and c2 not in i.guess or c1 not in i.guess and c2 in i.guess:
                    a+=3
            if a==2:
                for i in board.get_column(n):
                    if c1 in i.guess and c2 in i.guess:
                        item=0
                        while item<len(i.guess):
                            if i.guess[item]!=c1 and i.guess[item]!=c2:
                                i.guess.remove(i.guess[item])
                            else:
                                item+=1
            a=0
        for n,m in [(x,y) for x in range(3) for y in range(3)]:
            #même chose pour les occurences dans un square
            for i in board.get_square(n,m):
                if c1 in i.guess and c2 in i.guess:
                    a+=1
                if c1 in i.guess and c2 not in i.guess or c1 not in i.guess and c2 in i.guess:
                    a+=3
            if a==2:
                for i in board.get_square(n,m):
                    if c1 in i.guess and c2 in i.guess:
                        item=0
                        while item<len(i.guess):
                            if i.guess[item]!=c1 and i.guess[item]!=c2:
                                i.guess.remove(i.guess[item])
                            else:
                                item+=1
            a=0
    #3-uple checking part
    #même concept mais avec un trouple, le checking est seulement plus long
    for c1,c2,c3 in ((x,y,z) for x in range(1,10) for y in range(1,10) for z in 
            range(1,10) if x>y and y>z):
        for n in range(9):
            for i in board.get_row(n):
                if c1 in i.guess and c2 in i.guess and c3 in i.guess:
                    a+=1
            if a==2:
                for i in board.get_row(n):
                    if c1 in i.guess and c2 in i.guess and c3 in i.guess:
                        item=0
                        while item<len(i.guess):
                            if (i.guess[item]!=c1 and i.guess[item]!=c2 
                                    and i.guess[item]!=c3):
                                i.guess.remove(i.guess[item])
                            else:
                                item+=1
            a=0
            for i in board.get_column(n):
                if c1 in i.guess and c2 in i.guess and c3 in i.guess:
                    a+=1
            if a==2:
                for i in board.get_column(n):
                    if c1 in i.guess and c2 in i.guess and c3 in i.guess:
                        item=0
                        while item<len(i.guess):
                            if (i.guess[item]!=c1 and i.guess[item]!=c2 
                                    and i.guess[item]!=c3):
                                i.guess.remove(i.guess[item])
                            else:
                                item+=1
            a=0
        for n,m in [(x,y) for x in range(3) for y in range(3)]:
            for i in board.get_square(n,m):
                if c1 in i.guess and c2 in i.guess and c3 in i.guess:
                    a+=1
            if a==2:
                for i in board.get_square(n,m):
                    if c1 in i.guess and c2 in i.guess and c3 in i.guess:
                        item=0
                        while item<len(i.guess):
                            if (i.guess[item]!=c1 and i.guess[item]!=c2 
                                    and i.guess[item]!=c3):
                                i.guess.remove(i.guess[item])
                            else:
                                item+=1
            a=0

                           

def rule34(board):
    """rule 3 regarde si il existe des pavés orientés de guess, qui
    éliminent alors le carré non-concerné de deux lignes (ou colonnes)
    de ses guess pour le chiffre concerné"""
    
    """rule 4 regarde si il existe une ligne ou une colonne orientée
    (donc ici ce sera une seule ligne/colonne, à la différence du pavé)
    pour éliminer les autres guess dans cette ligne ou colonne"""
    
    """rule 3 est caractérisée par ses checks consécutifs du type
    0,1,1 par ex, rule 4 est caractérisée par ses checks consécutifs
    du type 0,0,1 par ex"""
    
    mod = 0
    
    cont,cfg={0:0,1:0,2:0},{0:0,1:0,2:0}
    for c in range(1,10):
        #row checking
        for n in range(3):
            #checks if c is a value in one the cells 
            #in the square row we're searching
            if c not in (x.value for i in range(3) 
                    for x in board.get_square(n,i)):
                for m in range(3):
                    for i in range(3*n,3*n+3):
                        #checks if c is one of the guesses in the 
                        #i-th row of the square we're searching
                        if c in (element for j in range(3*m,3*m+3) 
                                for element in board[i,j].guess):
                            cont[i%3]=1
                    if (cont[0],cont[1],cont[2]) == (1,1,0):
                        cfg[m]=1
                    elif (cont[0],cont[1],cont[2]) == (0,1,1):
                        cfg[m]=2
                    elif (cont[0],cont[1],cont[2]) == (1,0,1):
                        cfg[m]=3
                    else:
                        cfg[m]=0
                    cont={0:0,1:0,2:0}
                #checker les différentes configurations des carres obtenues 
                #precedemment 
                if cfg[0]==cfg[1] and cfg[0]!=0:
                    for i in (board[x,y] for y in range(6,9) for x in 
                            [(cfg[0]-1)%3,cfg[0]%3]):
                        liste=i.guess
                        try:
                            liste.remove(c)
                            mod = 1
                        except ValueError: pass
                        i.set_guess(liste)
                if cfg[2]==cfg[1] and cfg[1]!=0:
                    for i in (board[x,y] for y in range(0,3) for x in 
                            [(cfg[0]-1)%3,cfg[0]%3]):
                        liste=i.guess
                        try:
                            liste.remove(c)
                            mod = 1
                        except ValueError: pass
                        i.set_guess(liste)
                if cfg[0]==cfg[2] and cfg[0]!=0:
                    for i in (board[x,y] for y in range(3,6) for x in 
                            [(cfg[0]-1)%3,cfg[0]%3]):
                        liste=i.guess
                        try:
                            liste.remove(c)
                            mod = 1
                        except ValueError: pass
                        i.set_guess(liste)
            cfg={0:0,1:0,2:0}
            
            for m in range(3):
                if c not in [x.value for x in board.get_square(n,m)]:
                    for i in range(3):
                        if c in (element for j in range(3) 
                                for element in board[3*n+i,3*m+j].guess):
                            cont[i]=1
                    if (cont[0],cont[1],cont[2]) == (1,0,0):
                        #elimine sans passer par les config
                        for i in (x for x in board.get_row(3*n) 
                                if x not in board.get_square(n,m)):
                            liste=i.guess
                            try:
                                liste.remove(c)
                                mod = 1
                            except ValueError: pass
                            i.set_guess(liste)                            
                    elif (cont[0],cont[1],cont[2]) == (0,1,0):
                        for i in (x for x in board.get_row(3*n+1) 
                                if x not in board.get_square(n,m)):
                            liste=i.guess
                            try:
                                liste.remove(c)
                                mod = 1
                            except ValueError: pass
                            i.set_guess(liste)     
                    elif (cont[0],cont[1],cont[2]) == (0,0,1):
                        for i in (x for x in board.get_row(3*n+2) 
                                if x not in board.get_square(n,m)):
                            liste=i.guess
                            try:
                                liste.remove(c)
                                mod = 1
                            except ValueError: pass
                            i.set_guess(liste)     
                    cont[0],cont[1],cont[2]=0,0,0
                    #plus qu'à checker ça en colonne et rule34 est finie
                    
        #column checking            
        for m in range(3):
            #checks if c is a value in one the cells in the square column we're 
            #searching
            if c not in (x.value for i in range(3) for x in 
                    board.get_square(i,m)):
                for n in range(3):
                    for i in range(3*m,3*m+3):
                        #checks if c is one of the guesses in the i-th column 
                        #of the square we're searching
                        if c in (element for j in range(3*n,3*n+3) for element 
                                in board[j,i].guess):
                            cont[i%3]=1
                    if (cont[0],cont[1],cont[2]) == (1,1,0):
                        cfg[m]=1
                    elif (cont[0],cont[1],cont[2]) == (0,1,1):
                        cfg[m]=2
                    elif (cont[0],cont[1],cont[2]) == (1,0,1):
                        cfg[m]=3
                    else:
                        cfg[m]=0
                    cont={0:0,1:0,2:0}
                #checker les différentes configurations des carres obtenues 
                #precedemment 
                if cfg[0]==cfg[1] and cfg[0]!=0:
                    for i in (board[y,x] for y in range(6,9) for x in 
                            [(cfg[0]-1)%3,cfg[0]%3]):
                        liste=i.guess
                        try:
                            liste.remove(c)
                            mod = 1
                        except ValueError: pass
                        i.set_guess(liste)
                if cfg[2]==cfg[1] and cfg[1]!=0:
                    for i in (board[y,x] for y in range(0,3) for x in 
                            [(cfg[0]-1)%3,cfg[0]%3]):
                        liste=i.guess
                        try:
                            liste.remove(c)
                            mod = 1
                        except ValueError: pass
                        i.set_guess(liste)
                if cfg[0]==cfg[2] and cfg[0]!=0:
                    for i in (board[y,x] for y in range(3,6) for x in 
                            [(cfg[0]-1)%3,cfg[0]%3]):
                        liste=i.guess
                        try:
                            liste.remove(c)
                            mod = 1
                        except ValueError: pass
                        i.set_guess(liste)
            cfg={0:0,1:0,2:0}
            
            for n in range(3):
                if c not in [x.value for x in board.get_square(n,m)]:
                    for i in range(3):
                        if c in (element for j in range(3) 
                                for element in board[3*n+j,3*m+i].guess):
                            cont[i]=1
                    if (cont[0],cont[1],cont[2]) == (1,0,0):
                        #elimine sans passer par les config
                        for i in (x for x in board.get_column(3*m) 
                                if x not in board.get_square(n,m)):
                            liste=i.guess
                            try:
                                liste.remove(c)
                                mod = 1
                            except ValueError: pass
                            i.set_guess(liste)                            
                    elif (cont[0],cont[1],cont[2]) == (0,1,0):
                        for i in (x for x in board.get_column(3*m+1) 
                                if x not in board.get_square(n,m)):
                            liste=i.guess
                            try:
                                liste.remove(c)
                                mod = 1
                            except ValueError: pass
                            i.set_guess(liste)     
                    elif (cont[0],cont[1],cont[2]) == (0,0,1):
                        for i in (x for x in board.get_column(3*m+2) if x not 
                                in board.get_square(n,m)):
                            liste=i.guess
                            try:
                                liste.remove(c)
                                mod = 1
                            except ValueError: pass
                            i.set_guess(liste)     
                    cont[0],cont[1],cont[2]=0,0,0
    return mod


def solve(board):
    """Try to solve a board using only the functions remove_guesses,
    check_guesses and rule1dot5.
    Returns a board for compatibility with solve_extreme but it modifies the
    board directly (since Board objects are mutable)"""
    init_guesses(board)
    completed = False
    while not completed:
        remove_guesses(board)
        completed = check_guesses(board)
        board.check_cell_error()
        if completed and not board.is_complete():
            rule1dot5(board)
            completed = check_guesses(board)
            board.check_cell_error()
    return board

def solve_hard(board):
    """Same as solve, but uses also rule34"""
    init_guesses(board)
    completed = False
    while not completed:
        remove_guesses(board)
        completed = check_guesses(board)
        board.check_cell_error()
        if completed and not board.is_complete():
            rule1dot5(board)
            completed = check_guesses(board)
            board.check_cell_error()
            if completed and not board.is_complete():
                changed_guess = True
                while changed_guess:
                    changed_guess = rule34(board)
                rule1dot5(board)
                completed = check_guesses(board)
                board.check_cell_error()
    return board

def solve_extreme(board):
    """Same as solve, and solve_hard, but if the board cannot be completed that
    way, a copy of the board is made, a cell of it containing only two guesses
    is selected, a value is choosen, and the copy is tried to be solved. In
    case it fails, the other value is selected and we solve the copy the same
    way. It is a recursive function.
    It seems to be solving anything."""
    init_guesses(board)
    completed = False
    while not completed:
        remove_guesses(board)
        completed = check_guesses(board)
        board.check_cell_error()
        if completed and not board.is_complete():
            rule1dot5(board)
            completed = check_guesses(board)
            board.check_cell_error()
            if completed and not board.is_complete():
                changed_guess = True
                while changed_guess:
                    changed_guess = rule34(board)
                rule1dot5(board)
                completed = check_guesses(board)
                board.check_cell_error()
                if completed and not board.is_complete():
                    found = False
                    for i in range(9):
                        for j in range(9):
                            if len(board[i,j].guess) == 2:
                                clone = board.copy()
                                # Set clone value
                                clone[i,j].set_value(board[i,j].guess[1])
                                clone[i,j].set_mutable(False)
                                clone[i,j].set_guess(list())
                                # Set board value
                                board[i,j].set_value(board[i,j].guess[0])
                                board[i,j].set_mutable(False)
                                clone[i,j].set_guess(list())
                                try:
                                    board = solve_extreme(clone)
                                except ValueError:
                                    board = solve_extreme(board)
                                found = True
                            if found: break
                        if found: break
    return board


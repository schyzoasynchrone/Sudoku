#!/usr/bin/env python3
# encoding:utf-8

import pytest
from core.board import Board

@pytest.fixture
def board():
    return Board()

@pytest.fixture
def config():
    return ("351000070\n"
          "400502930\n"
          "200047560\n"
          "020070006\n"
          "006850092\n"
          "045000107\n"
          "034001025\n"
          "518020600\n"
          "000465800\n")

@pytest.fixture
def wrong_config():
    return ("321000070\n"
          "400502930\n"
          "200047560\n"
          "020070006\n"
          "006850092\n"
          "045000107\n"
          "034001025\n"
          "518020600\n"
          "000465800\n")

@pytest.fixture
def configured_board(config):
    board = Board()
    board.parse_config(config)
    return board

def test_parse_config(board, config):
    board.parse_config(config)
    board.check_cell_error()
    assert str(board) == config

def test_get_row(configured_board):
    cells = configured_board.get_row(0)
    row = list("351000070")
    for i in range(9):
        assert int(row[i]) == cells[i].value

def test_get_column(configured_board):
    cells = configured_board.get_column(5)
    column = (0,2,7,0,0,0,1,0,5)
    for i in range(9):
        assert column[i] == cells[i].value

def test_get_square(configured_board):
    cells = configured_board.get_square(0,0)
    square = (3,5,1,4,0,0,2,0,0)
    for i in range(9):
        assert square[i] == cells[i].value

def test_detect_errors(board, wrong_config):
    board.parse_config(wrong_config)
    with pytest.raises(ValueError):
        board.check_cell_error()

def test_iter(configured_board):
    i = 0
    for cell in configured_board:
        assert configured_board[(i//9,i%9)] == cell
        i += 1

if __name__ == "__main__":
    board = Board()
    board.parse_config(CONFIG)
    print(board)

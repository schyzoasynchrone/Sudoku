#!/usr/bin/env python3
# encoding:utf8

import pytest
from core.cell import Cell

@pytest.fixture
def cell():
    return Cell()

def test_set_value(cell):
    value = 4
    cell.set_value(value)
    assert cell.value == value

def test_set_guess(cell):
    guess = [2, 3, 5]
    cell.set_guess(guess)
    assert cell.guess == guess

def test_wrong_value(cell):
    with pytest.raises(ValueError):
        cell.set_value(10)

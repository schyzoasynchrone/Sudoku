#!/usr/bin/env python3
# encoding:utf8

import pytest
from core.board import Board
from solver import (init_guesses, remove_guesses, check_guesses, rule34,
        rule1dot5, rule2, solve, solve_hard, solve_extreme)

@pytest.fixture
def board():
    return Board()

@pytest.fixture
def simple_config():
    return ("000901020\n"
            "010060000\n"
            "290030408\n"
            "001507204\n"
            "085020170\n"
            "402308900\n"
            "107050046\n"
            "000040050\n"
            "050203000\n")

@pytest.fixture
def simple_config_solved():
    return ("543981627\n"
            "718462539\n"
            "296735418\n"
            "361597284\n"
            "985624173\n"
            "472318965\n"
            "127859346\n"
            "839146752\n"
            "654273891\n")

@pytest.fixture
def medium_config():
    return ("726004000\n"
            "000900040\n"
            "900700021\n"
            "003100409\n"
            "090040070\n"
            "402009100\n"
            "510008004\n"
            "030007000\n"
            "000500832\n")

@pytest.fixture
def medium_config_solved():
    return ("726314598\n"
            "381925647\n"
            "954786321\n"
            "863172459\n"
            "195843276\n"
            "472659183\n"
            "519238764\n"
            "238467915\n"
            "647591832\n")

@pytest.fixture
def hard_config():
    return ("503900000\n"
            "060050004\n"
            "020800600\n"
            "008170060\n"
            "000000000\n"
            "030025100\n"
            "002003040\n"
            "900080050\n"
            "000001902\n")

@pytest.fixture
def hard_config_solved():
    return ("573946218\n"
            "861352794\n"
            "429817635\n"
            "248179563\n"
            "195638427\n"
            "736425189\n"
            "652793841\n"
            "917284356\n"
            "384561972\n")

@pytest.fixture
def extrem_config():
    return ("800000000\n"
            "003600000\n"
            "070090200\n"
            "050007000\n"
            "000045700\n"
            "000100030\n"
            "001000068\n"
            "008500010\n"
            "090000400\n")

@pytest.fixture
def simple_conf_board(board, simple_config):
    board.parse_config(simple_config)
    board.check_cell_error()
    return board

@pytest.fixture
def medium_conf_board(board, medium_config):
    board.parse_config(medium_config)
    board.check_cell_error()
    return board

@pytest.fixture
def hard_conf_board(board, hard_config):
    board.parse_config(hard_config)
    board.check_cell_error()
    return board

@pytest.fixture
def extrem_conf_board(board, extrem_config):
    board.parse_config(extrem_config)
    board.check_cell_error()
    return board

def test_solve1(simple_conf_board, simple_config_solved):
    solve(simple_conf_board)
    assert str(simple_conf_board) == simple_config_solved

def test_medium_34(medium_conf_board, medium_config_solved):
    solve(medium_conf_board)
    assert str(medium_conf_board) == medium_config_solved

def test_hard_1dot5(hard_conf_board, hard_config_solved):
    solve_hard(hard_conf_board)
    assert str(hard_conf_board) == hard_config_solved

#def test_extrem_1dot5_34(extrem_conf_board, hard_config_solved):
#    extrem_conf_board = solve_extreme(extrem_conf_board)
#    assert str(extrem_conf_board) == hard_config_solved

